var width = window.innerWidth;

var screenWidth = window.innerWidth || document.documentElement.clientWidth;
var screenHeight = window.innerHeight || document.documentElement.clientHeight;
var mobileWidthLimit = 768;

$(function() {

  // Check if it is iOS
  var isiOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false);
  if(isiOS === true) {
    var tempCSS = $('a').css('-webkit-tap-highlight-color');
    $('main').css('cursor', 'pointer')
      .css('-webkit-tap-highlight-color', 'rgba(0, 0, 0, 0)');
    $('a').css('-webkit-tap-highlight-color', tempCSS);
  }

  $(".ripple").on("click", function(event) {
    var _this = $(this),
      offset = $(this).offset(),
      positionX = (event.pageX - offset.left),
      positionY = (event.pageY - offset.top);
    _this.append("<span class='ripple-effect'>");
    _this.find(".ripple-effect").css({
      left: positionX,
      top: positionY
    }).animate({
      opacity: 0,
    }, 1500, function() {
      $(this).remove();
    });
  });

  $(".ripple").hover(function() {
    var _this = $(this),
      offset = $(this).offset(),
      positionX = (event.pageX - offset.left),
      positionY = (event.pageY - offset.top);
    _this.append("<span class='ripple-effect'>");
    _this.find(".ripple-effect").css({
      left: positionX,
      top: positionY
    }).animate({
      opacity: 0,
    }, 1500, function() {
      $(this).remove();
    });
  }, function() {});

  $(".main_mnu_btn").on("click", function() {
    $(this).closest(".header").find(".mnu_list").toggleClass("active");
  });

  $(".lang_toggle_mobile_current").on("click", function() {
    $(this).closest(".lang_toggle_mobile").find(".lang_toggle_mobile_list").toggleClass("active");
  });

  /*$(".lang_toggle_mobile_list li a").on("click", function(e) {
   e.preventDefault();
   var _this = $(this),
   currentLi = _this.text(),
   container = _this.closest(".lang_toggle_mobile"),
   current = container.find(".lang_toggle_mobile_current"),
   currentText = current.text();
   _this.text(currentText);
   current.text(currentLi);

   location.href = $(this).attr('href');
   });*/

  /*$(".what_get_carusel").owlCarousel({
   items: 1,
   loop: true,
   margin: 0,
   nav: true,
   navText: ['<svg viewBox="0 0 5 8"><use xlink:href="#ic-arrow-g-left"></use></svg>', '<svg viewBox="0 0 5 8"><use xlink:href="#ic-arrow-g-right"></use></svg>'],
   dots: false
   });*/

  $(".travel_dates_accordeon_trigger").on("click", function (e) {
    e.preventDefault();

    var $this = $(this),
      item = $this.closest(".travel_dates_accordeon_item"),
      list = $this.closest(".travel_dates_accordeon_list"),
      items = list.find(".travel_dates_accordeon_item"),
      content = item.find(".travel_dates_accordeon_inner"),
      otherContents = list.find(".travel_dates_accordeon_inner"),
      duration = 300;

    if (!item.hasClass("active")) {
      items.removeClass("active");
      item.addClass("active");
      otherContents.stop(true, true).slideUp(duration);
      content.stop(true, true).slideDown(duration);
    } else {
      content.stop(true, true).slideUp(duration);
      item.removeClass("active")
    }
  });

  $(".travel_day_show_all_btn .btn").on("click", function(e) {
    e.preventDefault();

    var _this = $(this),
      btn = _this.closest(".travel_day_show_all_btn"),
      item = _this.closest(".travel_day"),
      container = item.closest(".travel_days"),
      showAll = container.find(".travel_day_hidden");

    item.removeClass("travel_day_show_all");
    showAll.css("display", "block");
    btn.hide(0);

  });

  $(".accompaniment_carusel").owlCarousel({
    items: 1,
    loop: true,
    margin: 0,
    nav: true,
    navText: [
      '<svg viewBox="0 0 5 8"><use xlink:href="#ic-arrow-g-left"></use></svg>',
      '<svg viewBox="0 0 5 8"><use xlink:href="#ic-arrow-g-right"></use></svg>'
    ],
    dots: false
  });

  var related_travel_carusel = $(".related_travel_carusel");
  related_travel_carusel.owlCarousel({
    loop: true,
    margin: 30,
    nav: true,
    navText: [
      '<svg viewBox="0 0 5 8"><use xlink:href="#ic-arrow-g-left"></use></svg>',
      '<svg viewBox="0 0 5 8"><use xlink:href="#ic-arrow-g-right"></use></svg>'
    ],
    dots: false,
    responsive : {
      0 : {
        items: 1,
      },
      578 : {
        items: 2,
      },
      992 : {
        items: 3,
      },
      1200 : {
        items: 4,
      },
      1400 : {
        items: 5,
      }
    }
  });

  //travel popup feedback
  $(".ask-message, .ask").on('click', function (event) {
    if ($(this).attr('href') == '#') {
      event.preventDefault();
      $('#popup-feedback-overlay').attr('disabled', true);
      $('#popup-feedback-overlay').fadeIn(400, function () { // после выполнения предъидущей анимации
        var str = $('#popup-feedback-form').css('width');
        var popupWidth = Number(str.substr(0, (str.length - 2)));
        var top = (popupWidth <= 680) ? '5%' : '10%';
        $('#popup-feedback-form')
          .css('display', 'block') // убираем у модального окна display: none;
          .animate({opacity: 1, top: top}, 200, 'linear', function () {
            $('#popup-feedback-overlay').attr('disabled', false);
          });

      });

      return false;
    } else {
      return true;
    }
  });
  $('#popup-feedback-close, #popup-feedback-overlay, .close-popup-btn').click(function () { // ловим клик по крестику или подложке
    if ($("#popup-feedback-overlay").attr('disabled') != 'disabled') {
      var str = $('#popup-feedback-form').css('width');
      var popupWidth = Number(str.substr(0, (str.length - 2)));
      var animate = (popupWidth <= 680) ? false : true;
      if (animate) {
        $('#popup-feedback-form')
          .animate({opacity: 0, top: '5%'}, 200,  // плавно меняем прозрачность на 0 и одновременно двигаем окно вверх
            function () { // после анимации
              $(this).css('display', 'none'); // делаем ему display: none;
              $('#popup-feedback-overlay').fadeOut(400); // скрываем подложку
              $("#popup-content-wrapper").show();
              $("#popup-success-result").hide();
            }
          );
      } else {
        $('#popup-feedback-form')
          .animate({opacity: 0, top: '0%'}, 200,  // плавно меняем прозрачность на 0 и одновременно двигаем окно вверх
            function () { // после анимации
              $(this).css('display', 'none'); // делаем ему display: none;
              $('#popup-feedback-overlay').fadeOut(400); // скрываем подложку
              $("#popup-content-wrapper").show();
              $("#popup-success-result").hide();
            }
          );
      }
    }

    return false;
  });
  fixPopupFeedback();

  travelDays();
  formingHrefTel();

});

$(document).mouseup(function (e) {
  var container = $(".mnu_list");
  if ($(e.target).closest(".mnu_list").length) return;
  if ($(e.target).closest(".main_mnu_btn").length) return;
  container.removeClass("active");
  e.stopPropagation();
});
$(document).mouseup(function (e) {
  var container = $(".lang_toggle_mobile_list");
  if ($(e.target).closest(".lang_toggle_mobile_current").length) return;
  if ($(e.target).closest(".lang_toggle_mobile_current").length) return;
  container.removeClass("active");
  e.stopPropagation();
});

function fixPopupFeedback() {
  var popup = $('#popup-feedback-form');
  if (screenWidth <= mobileWidthLimit) {
    if (popup.height() > screenHeight) {
      popup.height(screenHeight * 0.9 - 15); //-5% top and bottom
      popup.find('#popup-content-wrapper').height(popup.height() - 30); //- padding
    } else {
      popup.css('height', 'auto');
      popup.find('#popup-content-wrapper').css('height', 'auto');
    }
  } else {
    popup.css('height', 'auto');
    popup.find('#popup-content-wrapper').css('height', 'auto');
  }
}

function headerFixed() {

  var header = $(".header");

  if ($(this).scrollTop() > 1) {
    header.addClass("header-fixed");
  } else {
    header.removeClass("header-fixed");
  }

  $(window).on("scroll", function () {
    if ($(this).scrollTop() > 1) {
      header.addClass("header-fixed");
    } else {
      header.removeClass("header-fixed");
    }
  });

}

function travelDays() {

  travelDaysScroll()

  $(window).on("resize", function () {
    travelDaysScroll();
  });

}

var can_show = true;

function travelDaysScroll() {
  var idSection = $("#what_get"),
    menu = $(".menu_fixed"),
    offsetMnu = menu.height() + 60;


  $(window).on("scroll", function () {
    var fromTop = $(this).scrollTop(),
      offsettop = idSection.offset().top;

    if (offsettop-offsetMnu <= fromTop && can_show) {
      menu.addClass("active");
    } else {
      menu.removeClass("active");
    }
    /*if (offsettop-offsetMnu <= fromTop && (idSection.height() + offsettop) >= fromTop) {
     menu.addClass("active");
     } else {
     menu.removeClass("active");
     }*/

  });
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
function formingHrefTel() {

  var linkAll = $('.formingHrefTel'),
    joinNumbToStringTel = 'tel:+';

  $.each(linkAll, function () {
    var _this = $(this),
      linkValue = _this.text(),
      arrayString = linkValue.split("");


    for (var i = 0; i < arrayString.length; i++) {
      var thisNunb = isNumber(arrayString[i]);
      if (thisNunb === true || (arrayString[i] === "+" && i === 0)) {
        joinNumbToStringTel += arrayString[i];
      }
    }

    _this.attr("href", function () {
      return joinNumbToStringTel;
    });
    joinNumbToStringTel = 'tel:+'

  });

}
$(document).ready(function () {
  //hide reserve button
  /*var os = new OnScreen();
   menu = $(".menu_fixed"),
   os.on('enter', '#travel_dates_box', function () {
   menu.removeClass("active");
   can_show= false;
   console.log('hide reserve');
   });

   os.on('leave', '#travel_dates_box', function () {
   menu.addClass("active");
   can_show = true;
   console.log('show reserve');
   });*/
});

