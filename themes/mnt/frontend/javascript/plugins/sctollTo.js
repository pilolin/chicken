/**
 * Created by wentris on 07.04.17.
 */
$.scrollTo = $.fn.scrollTo = function (x, y, options) {
  if (!(this instanceof $)) return $.fn.scrollTo.apply($('html, body'), arguments);

  options = $.extend({}, {
    gap: {
      x: 0,
      y: 0
    },
    animation: {
      easing: 'swing',
      duration: 600,
      complete: $.noop,
      step: $.noop
    }
  }, options);

  return this.each(function () {
    const elem = $(this);

    elem.stop().animate({
      scrollLeft: !isNaN(Number(x)) ? x : $(y).offset().left + options.gap.x,
      scrollTop: !isNaN(Number(y)) ? y : $(y).offset().top + options.gap.y
    }, options.animation);
  });
};
