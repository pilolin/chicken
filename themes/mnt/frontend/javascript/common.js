/**
 * Created by wentris on 30.01.17.
 */

// require('device.js/lib/device');

import '../sass/constructor.scss';

import '../../ext/jquery.nice-number';
import App from './components/App';
import { deviceType, checkDeviceType, staticInit } from './utils/checkDeviceType';
import throttle from './utils/throttleResize';

window.$ = $;
window.jQuery = jQuery;
// device.landscape();


const MQ = deviceType();
const MNTApp = new App(MQ);
const $window = $(window);
const $document = $(document);

let isMobile = true;
let isDesktop = false;

throttle('resize', 'optimizedResize');

function switchDeviceType(mq) {
  if (mq === 'desktop' && isDesktop) {
    isDesktop = false;
    isMobile = true;
  } else if (mq === 'mobile' && isMobile) {
    isMobile = false;
    isDesktop = true;
  }
}

$document.on('ready', [
  MNTApp.init(),
  staticInit(MQ, MNTApp.switchToDesktop, MNTApp.switchToMobile)
]);

$window
  .on('optimizedResize', () => {
    const mq = deviceType();

    checkDeviceType(
      mq,
      isMobile,
      isDesktop,
      [MNTApp.switchToDesktop, MNTApp.switchToMobile],
    );
    switchDeviceType(mq);
  });

(
  () => {
    // function loadMapModule() {
    //   require.ensure([], require => {
    //     try {
    //       const mapModule = require('./routs/map');
    //
    //       mapModule();
    //     } catch (env) {
    //       console.log(env);
    //     }
    //   });
    // }

    // if ($('#partnersMap').length) loadMapModule();
  }
)();

