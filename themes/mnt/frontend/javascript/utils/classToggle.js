/**
 * Created by wentris on 03.02.17.
 */
window.classToggle = selector => {
  const elements = document.querySelectorAll(selector);
  const processElement = index => {
    const el = elements[index];
    
    // el['data-spoiler-state'] = 'shrouded';
    el.addEventListener('mouseover', () => {
      el.parentElement.classList.add('active');
    });
    el.addEventListener('mouseout', () => {
      el.parentElement.classList.remove('active');
    });
  };

  elements.forEach((el, i) => processElement(i));
};

