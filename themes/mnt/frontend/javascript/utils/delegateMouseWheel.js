/**
 * Created by wentris on 21.01.17.
 */
const wheelEvt = () => {
  if ('onwheel' in document.createElement('div')) {
    return 'wheel';
  }
  return document.onmousewheel !== undefined ? 'mousewheel' : 'DOMMouseScroll';
};

export default wheelEvt();
