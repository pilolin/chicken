/**
 * Created by wentris on 19.01.17.
 */
import $ from 'jquery';

const checkHeader = function checkHeader(section, changingEl, distance = 0) {
  if ($(section).is(`:in-viewport(${distance})`)) {
    $(changingEl).addClass('inverse');
    console.log('in');
  } else {
    $(changingEl).removeClass('inverse');
    console.log('out');
  }
};

export default checkHeader;
