/**
 * Created by wentris on 23.01.17.
 */
export function deviceType() {
  return window
    .getComputedStyle(document.querySelector('body'), '::before')
    .getPropertyValue('content').replace(/'/g, '').replace(/"/g, '');
}
export function checkDeviceType(MQ, isMobile, isDesktop, arrCbs) {
  if (MQ === 'desktop' && isDesktop) {
    arrCbs[0]();
  } else if (MQ === 'mobile' && isMobile) {
    arrCbs[1]();
  }
}

export function staticInit(mq, firstFunc, secFunc) {
  if (mq === 'desktop') {
    firstFunc();
  } else if (mq === 'mobile') {
    secFunc();
  }
}
