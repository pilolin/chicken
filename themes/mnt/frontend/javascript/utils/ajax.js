/**
 * Created by wentris on 17.03.17.
 */
const host = window.location.origin;
const ajaxConfig = {
  url: `${host}/api/`,
  getPartners: 'getPartners',
  getCities: 'getCities',
  getCountryPartners: 'getPartnersByCountryId'
};
const preloader = $('<img class="preloader" src="/themes/anex/frontend/icons/ajax-loader.gif"/>');

function composeLoadingText(src = 'Loading...') {
  const letters = src.split('').map((letter, indexOfLetter) => {
    return `<div style="animation-delay: ${indexOfLetter * 0.06}s" class="char">${letter}</div>`;
  });

  return `<div class="text-loader">${letters.join('')}</div>`;
}

function composeQuery(mainQuery, typeId, id, fn) {
  $.ajax({
    type: 'POST',
    url: mainQuery,
    data: {
      [typeId]: id
    },
    dataType: 'json',
    success: data => {
      if (data !== null || data !== 'undefined' || data.length > 0) {
        if (typeof fn === 'function' && fn) fn(data);
      }
    }
  });
}

function fillSelect(data, citySelectId, key) {
  const $select = typeof citySelectId === 'string' ? $(citySelectId) : citySelectId;

  const options = data.map(city => {
    if (key === 'name') {
      const cityName = city[key].toLowerCase();

      if (cityName.match(/online-store|интернет-магазины|интернет - магазины|інтернет магазини/)) {
        return `<option value="${city.id}" data-icon="icon-global">${cityName}</option>`;
      }
      return `<option value="${city.id}">${cityName}</option>`;
    } else if (key === 'title') {
      return `<option value="${city.id}">${city[key]}</option>`;
    }
  });

  $select.append(options);
  $select.selectpicker('refresh');
}

function setPreloader(target) {
  target.parents('.form-controls').addClass('keep-loading');
}

function removePreloader(target) {
  target.parents('.form-controls').removeClass('keep-loading');
}

export { ajaxConfig, composeQuery, fillSelect, preloader, setPreloader, removePreloader, composeLoadingText };
