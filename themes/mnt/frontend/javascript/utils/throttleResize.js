/**
 * Created by wentris on 22.01.17.
 */
const throttle = (type, name, obj) => {
  let running = false;
  const object = obj || window;
  const func = () => {
    if (running) {
      return;
    }
    running = true;
    requestAnimationFrame(() => {
      object.dispatchEvent(new CustomEvent(name));
      running = false;
    });
  };

  object.addEventListener(type, func);
};

export default throttle;
