/**
 * Created by wentris on 22.01.17.
 */
import 'bootstrap/js/tab';
import 'bootstrap/js/tooltip';
import 'bootstrap/js/transition';
import 'bootstrap/js/popover';
import 'bootstrap/js/modal';
import 'bootstrap/dist/css/bootstrap.css';

import 'selectize/dist/css/selectize.css';
import 'selectize/dist/js/selectize.min';
import 'slick-carousel/slick/slick-theme.scss';
import 'slick-carousel/slick/slick.scss';
import 'slick-carousel';
import 'geocomplete';
import 'jquery-gmap';
import '../plugins/sticky';

const $html           = $('html');
const $body           = $html.find('body');

export default class App {

  initSpinners = () => {
    const $inputNet = $('.spinner');

    $inputNet.niceNumber();
  };

  switchToDesktop = () => {};

  switchToMobile = () => {};

  initSlick = () => {
    const $cartSlider = $('.cart-slider');

    $cartSlider.slick({
      dots: false,
      centerMode: false,
      arrows: true,
      variableWidth: true,
      infinite: false,
      responsive: [
        {
          breakpoint: 600,
          settings: {
            centerMode: true,
            arrows: false
          }
        }
      ]
    });
  };

  handleLeaveSection = (origin, destination) => {
    const $indexHeader = $body.find('.index__header-info__wrapper');
    const HIDDEN_CLASS = 'is-hidden';

    if (destination.index === 0) {
      $indexHeader.addClass(HIDDEN_CLASS);
    } else {
      $indexHeader.removeClass(HIDDEN_CLASS);
    }
  };

  initFoodBlocks = () => {
    const ACTIVE_CLASS = 'is-active';
    const DISABLED_CLASS = 'is-disabled';
    const $foodBlock = $('.food-block');
    const $foodSwitcherLinks = $foodBlock.find('.food-block__switcher').find('input, .link');
    const $checkers = $('.food-block__checker');
    const $checkerTypeMainFood = $foodBlock.find('.food-block-type-main-food');
    const $slider = $foodBlock.find('.food-block__slider');
    const $extraAnchorTab = $('.food-block__nav li:nth-child(2) .link');
    const $removeExtraFood = $foodBlock.find('.remove-second-food a');

    const handleClickOnTab = (e) => {
      const $a = $(e.target);
      const textA = $(e.target).text();

      if (!$a.hasClass('is-disabled')) {
        const $siblings = $a.parents('.food-block__switcher').find(`.${ACTIVE_CLASS}`);
        const $tabPane = $a.parents('.tab-pane');
        const slideIndex = $a.parents('li').index();
        const tabIndex = $a.parents('.tab-pane').index();

        const $sliders = $a.parents('.food-block').find('.food-block__slider.is-active');

        e.preventDefault();

        if ($sliders.length > 0) {
          const $targetSlide = $($sliders[tabIndex]);

          $targetSlide.slick('slickGoTo', slideIndex);
        }

        $siblings.removeClass(ACTIVE_CLASS);
        $a.addClass(ACTIVE_CLASS);
        if ($tabPane.index() === 0) {
          const $tabContent = $a.parents('.tab-content').find('.tab-pane');
          let ownIndex;

          $.each($tabContent.find('li'), (i, e) => {
            if ($(e).find('a').text() === textA) {
              ownIndex = i;
              return false;
            }
          });

          if ($tabContent.length > 1) {
            const $dependedLinks = $($tabContent[1]).find('.food-block__switcher');
            const $rightSlider = $($sliders[1]);
            let nextSiblingIndex;

            $.each($dependedLinks.find('a'), (i, e) => {
              if ($(e).find('a').text() === textA) {
                ownIndex = i;
                return false;
              }
            });

            $dependedLinks
              .find(`li a`)
              .removeClass(DISABLED_CLASS);

            $dependedLinks
              .find(`li:nth-child(${ownIndex + 1}) a`)
              .addClass(DISABLED_CLASS)
              .attr('disabled', 'disabled');

            if ($dependedLinks.find(`li a.${ACTIVE_CLASS}`).hasClass(DISABLED_CLASS)) {
              if ($dependedLinks.find(`li a.${ACTIVE_CLASS}`).parent().next().length && !$dependedLinks.find(`li a.${ACTIVE_CLASS}`).parent().next().find('a').hasClass(DISABLED_CLASS)) {
                nextSiblingIndex = $dependedLinks
                                    .find(`li a.${ACTIVE_CLASS}`)
                                    .parent()
                                    .next()
                                    .index();
                $dependedLinks
                  .find('a')
                  .removeAttr('disabled', 'disabled')
                  .removeClass(ACTIVE_CLASS);
                $dependedLinks
                  .find(`li:nth-child(${nextSiblingIndex}) a`)
                  .children()
                  .removeAttr('disabled', 'disabled')
                  .removeClass(DISABLED_CLASS)
                  .addClass(ACTIVE_CLASS);
              } else {
                nextSiblingIndex = 0;
                $dependedLinks
                  .find('a')
                  .removeAttr('disabled', 'disabled')
                  .removeClass(ACTIVE_CLASS);
                $dependedLinks
                  .find('li:first-child a')
                  .removeAttr('disabled', 'disabled')
                  .addClass(ACTIVE_CLASS);
              }
              $rightSlider.slick('slickGoTo', nextSiblingIndex);
            }
          }
        }
      }
    };

    const handleSelectMainFood = (e) => {
      e.preventDefault();
      const $this = $(e.target);
      const $currentfoodBlock = $this.closest('.food-block');
      const targetId = $this.val();
      const $foodBlockElements = $currentfoodBlock.find('[data-type-main-food]');
      const $currentFoodBlockElements = $currentfoodBlock.find(`[data-type-main-food="${targetId}"]`);
      const $foodSliders = $currentfoodBlock.find(`.food-block__slider[data-type-main-food`);
      const $currentFoodSlider = $currentfoodBlock.find(`.food-block__slider[data-type-main-food="${targetId}"]`);

      $foodBlockElements.css({ position: 'absolute', opacity: 0, 'z-index': 1 });
      $foodSliders.removeClass(ACTIVE_CLASS);
      $currentFoodBlockElements
        .css({ position: 'relative', 'z-index': 5 })
        .animate({ opacity: 1 }, 150);
      $currentFoodSlider.addClass(ACTIVE_CLASS);
    };

    const handleClickOnChecker = (e) => {
      const $a = $(e.target);
      const $siblings = $a.parents('ul').find(`.${ACTIVE_CLASS}`);

      e.preventDefault();

      $siblings.removeClass(ACTIVE_CLASS);
      $a.addClass(ACTIVE_CLASS);
    };

    $foodSwitcherLinks.on('click', handleClickOnTab);

    $checkers.find('a').on('click', handleClickOnChecker);

    $checkerTypeMainFood.on('change', handleSelectMainFood);

    $extraAnchorTab.on('click', (e) => {
      const $this = $(e.target);
      const $blockInfo = $this.closest('.food-block__info, .constructor__left__tabs');

      $blockInfo.find('.mobile-souse-checker__extra input')
                .prop('checked', true)
                .trigger('input');
      $blockInfo.find('.remove-second-food')
                .show(200);
    });

    $removeExtraFood.on('click', (e) => {
      const $this = $(e.target);
      const $blockInfo = $this.closest('.food-block__info, .constructor__left__tabs');

      const $souseCheckerExtra = $blockInfo.find('.mobile-souse-checker__extra input');

      if ($souseCheckerExtra.length) {
        $souseCheckerExtra.prop('checked', false).trigger('input');
      } else {
        this.handleTabShown(false)(e);
      }

      $this.parent().hide(200);

      $blockInfo.find('.tab-pane')
                .removeClass('active');

      $blockInfo.find('.food-block__nav li:first-child')
                .tab('show');

      $blockInfo.find('.tab-pane:first-child')
                .addClass('active');
    });

    $slider.slick({
      dots: false,
      arrows: false,
      slidesToScroll: 1,
      slidesToShow: 1,
      fade: true,
      draggable: false,
      swipe: false,
      touchMove: false
    });

    setTimeout(() => {
      $slider.slick('refresh');
    }, 100);
  };

  handleTabShown = (isTabs) => (e) => {
    const $this = $(e.target);
    const $wrapper = $this.parents('.food-block');
    const EXTENSION_CLASS = 'cut-mode';
    // конструктор в десктопе
    const $wrapperConstructor = $this.parents('.constructor-changed-block .constructor__left__img');
    // мобильный конструктор
    const $wrapperConstructorMob = $this.parents('.checkout-step');

    if ($wrapperConstructor.length) {
      if ($this.prop('checked')) {
        $wrapperConstructor.addClass(EXTENSION_CLASS);
      } else {
        $wrapperConstructor.removeClass(EXTENSION_CLASS);
      }
    } else if ($wrapperConstructorMob.length) {
      if ($this.prop('checked')) {
        $wrapperConstructorMob.find('.food-block__slider-wrapper.is-active').addClass(EXTENSION_CLASS);
      } else {
        $wrapperConstructorMob.find('.food-block__slider-wrapper.is-active').removeClass(EXTENSION_CLASS);
      }
    } else {
      if ($this.prop('checked')) {
        $wrapper.addClass(EXTENSION_CLASS);
      } else {
        $wrapper.removeClass(EXTENSION_CLASS);
      }
    }

    $wrapper.find('.food-block__slider').slick('refresh');
  };

  initTooltips = () => {
    const $tooltips = $('[data-toggle="tooltip"]');
    const $popover = $('[data-toggle="popover"]');

    $tooltips.tooltip();
    $popover.popover();
  };

  initSelect2 = () => {
    const $select2 = $('.select2-custom');

    $select2.selectize({
      create: false,
      hideSelected: true,
      persist: false
    });
  };

  initDeliveryMaps = () => {
    const $maps = $body.find('.delivery-map');

    if ($maps.length > 0) {
      [ ...$maps ].forEach(node => {
        const $node = $(node);
        const lat = $node.data('lat');
        const lng = $node.data('lng');

        const map = $node.gmap({
          lat,
          lng,
          options: {
            zoom: 13
          }
        });

        map.gmap('addMarker', {
          lat,
          lng,
          icon: 'images/marker2.png'
        //  title: 'I am the title'
        //  infoWindow: {
        //    content: 'This is an info popup',
        //    opened: true
        //  }
        });
      });
    }
  };

  initCheckoutComments = () => {
    const $wrapper = $body.find('.checkout__comment');
    const $toggler = $wrapper.find('.link');
    const DURATION = 200;
    const OPEN_CLASS = 'is-open';

    $toggler.on('click', (e) => {
      const $parent = $(e.target).parents('.checkout__comment');
      const $panel = $parent.find('.checkout__comment__body');

      e.preventDefault();

      $parent.toggleClass(OPEN_CLASS);
      if ($parent.hasClass(OPEN_CLASS)) {
        $panel.show(DURATION);
      } else {
        $panel.hide(DURATION);
      }
    });
  };

  initMobileSouseChecker = () => {
    const $souseChecker = $body.find('.mobile-souse-checker');
    const $extraAnchor = $souseChecker.find('.mobile-souse-checker__anchor input');
    const $wrapper = $body.find('.food-block');
    const $mainSouseWrap = $wrapper.find('.mobile-souse-checker__main');
    const $extraSelectWrap = $wrapper.find('.mobile-souse-checker__extra__select');
    const $mainSelect = $mainSouseWrap.find('.select-custom select');
    const $extraSelect = $extraSelectWrap.find('.select-custom select');
    const $mobileConstructorDrink = $('.mobile-constructor .drink-mobile-select select');
    const $mobileConstructorFood = $('.mobile-constructor .food-mobile-select select');
    const $drinkTypesSliders = $('.mobile-constructor .drink-mobile-select + .checkout-step__img');
    const DURATION = 200;

    // $mainSelect.on('change', (e) => {
    //   const $this = $(e.target);
    //   const textSelectedOption = $this.find(`option[value=${$this.val()}]`).text();
    //   const descriptionText = $(e.target).find(`option[value=${e.target.value}]`).attr('data-description');
    //   const $description = $(e.target).parent().next('.mobile-souse-checker__description');
    //   let $foodBlockSlider;
    //   // для мобильного конструктора

    //   if ($this.closest('.checkout-stepper').length) {
    //     const selectedIndex = $mobileConstructorFood.prop('selectedIndex');
    //     $foodBlockSlider = $this.closest('.food-block').find(`.food-block__slider-wrapper:nth-child(${selectedIndex + 1}) .food-block__slider.left-mode`);
    //   } else {
    //     $foodBlockSlider = $this.closest('.food-block').find('.food-block__slider.left-mode');
    //   }
    //   const $extraSelectThisFood = $this.parents('.mobile-souse-checker').find('.mobile-souse-checker__extra__select select');

    //   let optionIndex = 0;

    //   $.each($extraSelectThisFood.find('option'), (i, e) => {
    //     if ($(e).text() === textSelectedOption) {
    //       optionIndex = i;
    //       return false;
    //     }
    //   });

    //   $extraSelectThisFood.find('option').removeAttr('disabled');
    //   $extraSelectThisFood.find(`option:nth-child(${optionIndex + 1})`).attr('disabled', 'disabled');

    //   if ($extraSelectThisFood.find('option:selected').attr('disabled')) {
    //     if ($extraSelectThisFood.find('option:selected').next().length && !$extraSelectThisFood.find('option:selected').next().attr('disabled')) {
    //       $extraSelectThisFood.find('option:selected').next().prop('selected', true);
    //     } else {
    //       $extraSelectThisFood.find('option:first-child').prop('selected', true);
    //     }
    //     $extraSelectThisFood.trigger('change');
    //   }

    //   $foodBlockSlider.slick('slickGoTo', e.target.value);
    //   $description.text(descriptionText);
    // });

    // $extraSelect.on('change', (e) => {
    //   const $this = $(e.target);
    //   const descriptionText = $(e.target).find(`option[value=${e.target.value}]`).attr('data-description');
    //   const $description = $(e.target).parent().next('.mobile-souse-checker__description');
    //   let $foodBlockSlider;
    //   if ($this.closest('.checkout-stepper').length) {
    //     const selectedIndex = $mobileConstructorFood.prop('selectedIndex');
    //     $foodBlockSlider = $this.closest('.food-block').find(`.food-block__slider-wrapper:nth-child(${selectedIndex + 1}) .food-block__slider.right-mode`);
    //   } else {
    //     $foodBlockSlider = $this.closest('.food-block').find('.food-block__slider.right-mode');
    //   }

    //   $foodBlockSlider.slick('slickGoTo', e.target.value);
    //   $description.text(descriptionText);
    // });

    $extraAnchor.on('input', (e) => {
      const $this = $(e.target);
      const $rootNode = $this.parents('.mobile-souse-checker');
      const $extraSouseWrap = $rootNode.find('.mobile-souse-checker__extra');
      const $extraSelectWrapInner = $extraSouseWrap.find('.mobile-souse-checker__extra__select');

      e.preventDefault();
      $extraSelectWrapInner.toggle(DURATION);
      this.handleTabShown(false)(e);
    });

    $mobileConstructorFood.on('change', (e) => {
      const $foodStep = $(e.currentTarget).closest('.checkout-step');
      const $foodStepSliderFood = $foodStep.find('.food-block__slider-container');
      const $foodStepActiveSliderFood = $foodStep.find('.food-block__slider-wrapper.is-active');
      const $foodStepSelectsFood = $foodStep.find('.food-block__info');
      const $foodStepActiveSelectsFood = $foodStep.find('.mobile-souse-checker.is-active');
      let selectedIndex = e.target.selectedIndex || 0;

      $foodStepActiveSliderFood.animate({ opacity: 0 }, 150, () => {
        $foodStepActiveSliderFood
          .css('position', 'absolute')
          .removeClass('is-active');
        $foodStepSliderFood
          .find(`.food-block__slider-wrapper:nth-child(${selectedIndex + 1})`)
          .addClass('is-active')
          .css('position', 'static');
        $foodStepSliderFood.find('.food-block__slider').slick('refresh');
        $foodStepSliderFood
          .find(`.food-block__slider-wrapper:nth-child(${selectedIndex + 1})`)
          .animate({ opacity: 1 }, 150);
      });
      $foodStepActiveSelectsFood.animate({ opacity: 0 }, 150, () => {
        $foodStepActiveSelectsFood
          .css('position', 'absolute')
          .removeClass('is-active');
        $foodStepSelectsFood
          .find(`.mobile-souse-checker:nth-child(${selectedIndex + 1})`)
          .css('position', 'static')
          .addClass('is-active')
          .animate({ opacity: 1 }, 150);
      });
    });

    $mobileConstructorDrink.on('change', (e) => {
      const $this = $(e.target);
      const val = $this.val();
      const $rootNode = $this.closest('.mobile-constructor-volumes');
      const $titleSelectedDrink = $rootNode.find('.mobile-constructor-volume__title');

      $titleSelectedDrink.text(val[0].toUpperCase() + val.substr(1));

      $drinkTypesSliders.slick('slickGoTo', e.target.selectedIndex);
    });

    $('#phoneCartModal').on('show.bs.modal', () => {
      setTimeout(() => {
        $drinkTypesSliders.slick({
          dots: false,
          arrows: false,
          slidesToScroll: 1,
          slidesToShow: 1,
          fade: true,
          draggable: false,
          swipe: false,
          touchMove: false
        }).on('setPosition', () => {
          $drinkTypesSliders.animate({ opacity: 1 }, 150);
        });
      }, 500);
    }).on('hide.bs.modal', () => {
      $('.mobile-constructor .food-block').css('opacity', 0);
      $drinkTypesSliders.slick('unslick');
      $drinkTypesSliders.css('opacity', 0);
    });
  };

  initDrinkMobileChecker = () => {
    const $wrapper = $body.find('.section').find('.drinks-mobile');
    const $carousel = $wrapper.find('.drinks-mobile__carousel');
    const $slider = $wrapper.find('.drinks-mobile__slider');

    // Init mobile sliders
    const $cartModal = $body.find('#phoneCartModal');
    const $innerCarousel = $cartModal.find('.drinks-mobile__carousel');
    const $innerSlider = $cartModal.find('.drinks-mobile__slider');

    const CAROUSEL_SETTINGS = {
      dots: false,
      arrows: false,
      variableWidth: true,
      infinite: true,
      mobileFirst: true,

      slidesToScroll: 1,
      slidesToShow: 2,

      centerMode: false,
      focusOnSelect: true
    };
    const SLIDER_SETTINGS = {
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true
    };

    $carousel.slick({
      ...CAROUSEL_SETTINGS,
      asNavFor: $slider
    });

    $slider.slick({
      ...SLIDER_SETTINGS,
      asNavFor: $carousel
    });

    $cartModal.on('shown.bs.modal', () => {
      $innerCarousel.slick({
        ...CAROUSEL_SETTINGS,
        asNavFor: $innerSlider
      });

      $innerSlider.slick({
        ...SLIDER_SETTINGS,
        asNavFor: $innerCarousel
      });
    }).on('hidden.bs.modal', () => {
      $innerCarousel.slick('unslick');
      $innerSlider.slick('unslick');
    });
  };

  initConstructor = () => {
    const $constructor = $body.find('.constructor');
    const $switchers = $constructor.find('.constructor__nav').find('.link');
    const $slider = $constructor.find('.constructor__slider');
    const $constructorChangedBlock = $constructor.find('.constructor-changed-block');

    $slider.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true
    });

    $constructorChangedBlock.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true
    });

    $switchers.on('click', (e) => {
      const $a = $(e.target);
      const $li = $a.parents('li');
      const index = $li.index();
      const $section = $li.parents('.constructor__section');
      const $changeBlock = $section.find('.constructor-changed-block');

      if ($changeBlock.length) {
        $changeBlock.slick('slickGoTo', index);
        $changeBlock.find('.food-block__slider').removeClass('is-active');
        $changeBlock.find(`[data-slick-index="${index}"] .constructor__left__img .food-block__slider`).addClass('is-active');
      } else {
        $section.find('.constructor__slider').slick('slickGoTo', index);
      }
    });

    const mql = window.matchMedia('only screen and (min-width: 1025px)'); 

    function setupForWidthLeftBlock() {
      $slider.slick('refresh');
    }
    
    mql.addListener(setupForWidthLeftBlock);
    setupForWidthLeftBlock(mql);
  };
  initStickyfill = () => {
    const elements = document.querySelectorAll('.sticky');

    Stickyfill.add(elements);
  }

  fixScrollBodyShowedModal = () => {
    $('.modal').on('show.bs.modal', () => {
      setTimeout(() => {
        if (!$body.attr('data-body-scroll-fix')) {
          const scrollPosition = window.pageYOffset || document.documentElement.scrollTop;

          $body
            .attr('data-body-scroll-fix', scrollPosition)
            .css({
              overflow: 'hidden',
              position: 'fixed',
              top: -1 * scrollPosition,
              left: 0,
              width: '100%'
            });
        }
      }, 10);
    });

    $('.modal').on('hide.bs.modal', () => {
      if ($body.attr('data-body-scroll-fix')) {
        const scrollPosition = $body.attr('data-body-scroll-fix');

        $body
          .removeAttr('data-body-scroll-fix')
          .css({
            overflow: 'visible',
            position: '',
            top: '',
            left: '',
            width: ''
          });
        window.scroll(0, scrollPosition);
      }
    });
  }

  callbackForModal = () => {
    $('.modal').on('show.bs.modal', (e) => {
      const $button = $(e.relatedTarget);

      if ($button.data('target') === '#selectCity') {
        $button.addClass('opened-modal');
      }
      
      if ($button.data('target') === '#phoneCartModal') {
        const $sliders = $('#phoneCartModal .food-block__slider');

        setTimeout(() => {
          $sliders
            .slick('refresh')
            .animate({ opacity: 1 }, 100);
        }, 500);
      }
    });

    $('.modal').on('hide.bs.modal', (e) => {
      const $button = $(`[data-target="#${e.currentTarget.id}"]`);

      if ($button.data('target') === '#selectCity') {
        $button.removeClass('opened-modal');
      }
    });
  }

  initToggleCityList = () => {
    const $modal = $('#selectCity');
    const $buttonCityList = $modal.find('header a');
    const $listDeliveryPlace = $modal.find('.list-delivery-places');
    const $listCities = $modal.find('.list-select-city');

    $buttonCityList.on('click', (e) => {
      e.preventDefault();
      $listDeliveryPlace.toggleClass('opened');
      $listCities.toggleClass('opened');
    });
  }

  initSticky = () => {
    $('header .index__header-info').sticky();
    $('header .index__header-info').on('sticky-start', () => {
      $('header .index__header-info__wrapper').removeClass('is-hidden');
    });
    $('header .index__header-info').on('sticky-end', () => {
      $('header .index__header-info__wrapper').addClass('is-hidden');
    });
  }

  initScrollToTwoSection = () => {
    $('.section-main .go-down').on('click', () => {
      const nextBlockTop = $('.section-main').outerHeight();

      $('html, body').animate({ scrollTop: nextBlockTop }, 300);
    });
  }

  init = () => {
    this.initConstructor();
    this.initMobileSouseChecker();
    this.initSlick();
    this.initSpinners();
    this.initTooltips();
    this.initFoodBlocks();
    this.initSelect2();
    this.initDeliveryMaps();
    this.initCheckoutComments();
    this.initDrinkMobileChecker();
    this.fixScrollBodyShowedModal();
    this.callbackForModal();
    this.initToggleCityList();
    this.initSticky();
    this.initScrollToTwoSection();
  };
}
